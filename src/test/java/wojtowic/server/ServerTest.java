package wojtowic.server;

import org.junit.Rule;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.junit.rules.Timeout;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertThrows;

class ServerTest {

    private Server s;

    @Rule
    public Timeout globalTimeout = Timeout.seconds(2);

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test()
    void serveOnNull() {
        Executable exec = () -> s.serve();
        assertThrows(java.lang.NullPointerException.class, exec, "Exception has been thrown");
    }

    @Test()
    void factory() throws IOException {
        s = Server.create(9998, 32);
    }

    @Test()
    void serve() throws IOException {
        s = Server.create(9999, 32);
        s.serve();
    }
}