package wojtowic.controller;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import wojtowic.model.ImageFeed;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class FeedsControllerTest {

    private FeedsController feedsController;

    @BeforeEach
    void setUp() {
        feedsController = new FeedsController();
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getFeed() {
        ImageFeed f1 = feedsController.getFeed(null);
        assertNull(f1);
        f1 = new ImageFeed(new byte[128]);
        feedsController.setFeed("k1", f1);
        ImageFeed f2 = feedsController.getFeed("k1");
        assertEquals(f1, f2);

    }

    @Test
    void setFeed() {
        ImageFeed f1 = null;
        feedsController.setFeed("k1", f1);
        assertEquals(f1, feedsController.getFeed("k1"));
    }
}