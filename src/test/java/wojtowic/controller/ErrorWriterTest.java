package wojtowic.controller;

import com.sun.net.httpserver.HttpExchange;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Rule;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import wojtowic.common.ErrorWriter;

import java.io.IOException;
import java.io.OutputStream;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ErrorWriterTest {

    private static Logger log = LogManager.getLogger(ErrorWriterTest.class);

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Test
    void create() {
        ErrorWriter w = new ErrorWriter();
    }

    @Test
    void setClientError() throws IOException {
        HttpExchange httpExchange = mock(HttpExchange.class);
        OutputStream os = mock(OutputStream.class);
        when(httpExchange.getResponseBody()).thenReturn(os);
        ErrorWriter.setClientError(httpExchange, "Client Error", log);
    }

    @Test
    void setServerError() throws IOException {
        HttpExchange httpExchange = mock(HttpExchange.class);
        OutputStream os = mock(OutputStream.class);
        when(httpExchange.getResponseBody()).thenReturn(os);
        ErrorWriter.setServerError(httpExchange, "Server Error", log);
    }
}