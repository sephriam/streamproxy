package wojtowic.common;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.net.URI;

import static org.junit.jupiter.api.Assertions.*;

class KeyHandlerTest extends KeyHandler {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getKey() {
        URI uri = URI.create("/subscribe/key1");
        String key = super.getKey(uri);
        assertEquals("key1", key);
    }

    @Test
    void getNullKey() {
        URI uri = URI.create("/subscribe/");
        String key = super.getKey(uri);
        assertNull(key);
    }
}