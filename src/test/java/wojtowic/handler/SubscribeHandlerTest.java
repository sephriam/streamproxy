package wojtowic.handler;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import org.junit.Rule;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import wojtowic.controller.FeedsController;
import wojtowic.model.ImageFeed;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class SubscribeHandlerTest {

    private SubscribeHandler handler;

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @BeforeEach
    void setUp() {
        handler = new SubscribeHandler(new FeedsController());
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void handle() throws IOException {
        HttpExchange httpExchange = mock(HttpExchange.class);
        OutputStream os = mock(OutputStream.class);
        when(httpExchange.getResponseBody()).thenReturn(os);
        Headers headers = new Headers();
        when(httpExchange.getResponseHeaders()).thenReturn(headers);

        URI uri = URI.create("/subscribe/key1");
        when(httpExchange.getRequestURI()).thenReturn(uri);
        handler.handle(httpExchange);

        uri = URI.create("/subscribe/");
        when(httpExchange.getRequestURI()).thenReturn(uri);
        handler.handle(httpExchange);

        handler.feedsController.setFeed("key1", new ImageFeed(new byte[128]));
        uri = URI.create("/subscribe/key1");
        when(httpExchange.getRequestURI()).thenReturn(uri);
        handler.handle(httpExchange);
    }
}