package wojtowic.handler;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import org.junit.Rule;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import wojtowic.controller.FeedsController;
import wojtowic.model.ImageFeed;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class FeedHandlerTest {

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    private FeedHandler handler;

    @BeforeEach
    void setUp() {
        handler = new FeedHandler(new FeedsController());
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void handle() throws IOException {
        HttpExchange httpExchange = mock(HttpExchange.class);
        OutputStream os = mock(OutputStream.class);
        when(httpExchange.getResponseBody()).thenReturn(os);
        Headers headers = new Headers();
        when(httpExchange.getResponseHeaders()).thenReturn(headers);
        InputStream is = mock(InputStream.class);
        when(httpExchange.getRequestBody()).thenReturn(is);
        when(is.read(any())).thenReturn(-1);

        URI uri = URI.create("/feed/key1");
        when(httpExchange.getRequestURI()).thenReturn(uri);
        handler.handle(httpExchange);

        uri = URI.create("/feed/");
        when(httpExchange.getRequestURI()).thenReturn(uri);
        handler.handle(httpExchange);

        handler.feedsController.setFeed("key1", new ImageFeed(new byte[128]));
        uri = URI.create("/feed/key1");
        when(httpExchange.getRequestURI()).thenReturn(uri);
        handler.handle(httpExchange);
    }
}