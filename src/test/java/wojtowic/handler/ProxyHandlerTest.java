package wojtowic.handler;

import com.sun.net.httpserver.HttpExchange;
import org.junit.Rule;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import wojtowic.exception.UpstreamException;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URI;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ProxyHandlerTest extends ProxyHandler {

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void malformedRequest() {
        MalformedURLException e = assertThrows(MalformedURLException.class,
                () -> openUpstream("htttp://localhost:9999"));
        assertEquals("unknown protocol: htttp", e.getMessage());
    }

    @Test
    void openUpstream() throws MalformedURLException {
        try {
            assertNotNull(openUpstream("http://localhost:9999"));
        } catch (UpstreamException e) {
            assertEquals("Error opening the upstream, http://localhost:9999", e.getMessage());
        }
    }

    @Test
    void getUpstream() {
        assertEquals("http://localhost:9999/jpeg",
                getUpstream("/proxy/?upstream=http://localhost:9999/jpeg&something=sth"));
        assertEquals("http://localhost:9999",
                getUpstream("/proxy/?upstream=http://localhost:9999"));
        assertNull(getUpstream("/proxy/?nostream=http://localhost:9999"));
    }

    @Test
    void readAndSend() throws IOException {
        InputStream is = mock(InputStream.class);
        OutputStream os = mock(OutputStream.class);
        when(is.read(any())).thenReturn(5).thenReturn(-1);
        readAndSend(is, os);
    }

    @Test
    void handleFlow() throws IOException {
        HttpExchange httpExchange = mock(HttpExchange.class);
        OutputStream os = mock(OutputStream.class);
        when(httpExchange.getResponseBody()).thenReturn(os);

        URI uri = URI.create("/proxy/?upstream=http://localhost:9999");
        when(httpExchange.getRequestURI()).thenReturn(uri);
        handle(httpExchange);

        uri = URI.create("/proxy");
        when(httpExchange.getRequestURI()).thenReturn(uri);
        handle(httpExchange);

        uri = URI.create("/proxy?upstream=htttp://asdasd");
        when(httpExchange.getRequestURI()).thenReturn(uri);
        handle(httpExchange);
    }
}