package wojtowic.common;

import com.sun.net.httpserver.HttpExchange;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

public class ErrorWriter {

    public static void setClientError(HttpExchange httpExchange, String msg, Logger log) throws IOException {
        log.warn("Client error " + msg);
        ErrorWriter.setClientError(httpExchange, msg);
    }

    public static void setServerError(HttpExchange httpExchange, String msg, Logger log) throws IOException {
        log.error("Server error " + msg);
        ErrorWriter.setServerError(httpExchange, msg);
    }

    public static void setClientError(HttpExchange httpExchange, String msg) throws IOException {
        httpExchange.sendResponseHeaders(400, msg.length());
        httpExchange.getResponseBody().write(msg.getBytes());
    }

    public static void setServerError(HttpExchange httpExchange, String msg) throws IOException {
        httpExchange.sendResponseHeaders(500, msg.length());
        httpExchange.getResponseBody().write(msg.getBytes());
    }
}
