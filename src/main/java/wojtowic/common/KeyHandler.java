package wojtowic.common;

import java.net.URI;

public class KeyHandler {

    protected String getKey(URI uri) {
        String[] split = uri.toString().split("/");
        String key = null;
        if (split.length > 2) {
            key = split[2];
        }

        return key;
    }
}
