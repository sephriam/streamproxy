package wojtowic.controller;

import wojtowic.model.ImageFeed;

import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class FeedsController {

    private Map<String, ImageFeed> feeds;
    private final ReadWriteLock rwLock;

    public FeedsController() {
        feeds = new TreeMap<>();
        rwLock = new ReentrantReadWriteLock();
    }

    public ImageFeed getFeed(String key) {

        if (key == null) {
            return null;
        }

        rwLock.readLock().lock();
        ImageFeed f = feeds.getOrDefault(key, null);
        rwLock.readLock().unlock();
        return f;
    }

    public void setFeed(String key, ImageFeed feed) {
        rwLock.writeLock().lock();
        this.feeds.put(key, feed);
        rwLock.writeLock().unlock();
    }
}
