package wojtowic.server;

import com.sun.net.httpserver.HttpServer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import wojtowic.controller.FeedsController;
import wojtowic.handler.FeedHandler;
import wojtowic.handler.ProxyHandler;
import wojtowic.handler.SubscribeHandler;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.Executors;

public class Server {

    private HttpServer server;
    private static Logger log = LogManager.getLogger(Server.class);

    private FeedsController feedsController;

    public static Server create(int port) throws IOException {
        Server ret = new Server();
        ret.server = HttpServer.create(new InetSocketAddress(port), 0);
        ret.feedsController = new FeedsController();
        ret.server.createContext("/proxy/", new ProxyHandler());
        ret.server.createContext("/feed/", new FeedHandler(ret.feedsController));
        ret.server.createContext("/subscribe/", new SubscribeHandler(ret.feedsController));

        return ret;
    }

    public static Server create(int port, int threadPoolSize) throws IOException {
        Server ret = create(port);
        ret.server.setExecutor(Executors.newFixedThreadPool(threadPoolSize));
        return ret;
    }

    public void serve() {
        log.info("Starting server at address " + server.getAddress());
        server.start();
    }
}
