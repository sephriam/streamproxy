package wojtowic.handler;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import wojtowic.common.ErrorWriter;
import wojtowic.exception.UpstreamException;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;

public class ProxyHandler implements HttpHandler {

    private static Logger log = LogManager.getLogger(ProxyHandler.class);

     String getUpstream(String uri) {
        String ret = null;
        int upstreamBegIdx = uri.indexOf("upstream=");
        int upstreamEndIdx = uri.indexOf("&", upstreamBegIdx);
        if(upstreamEndIdx == -1) {
            upstreamEndIdx = uri.length();
        }

        if (upstreamBegIdx != -1) {
            ret = uri.substring(upstreamBegIdx + "upstream=".length(), upstreamEndIdx);
        }

        return ret;
    }

    InputStream openUpstream(String upstream) throws MalformedURLException, UpstreamException {
        URL url = new URL(upstream);
        InputStream is;
        try {
            is = url.openStream();
        } catch (IOException e) {
            log.warn("Error opening the upstream\n" + e.getMessage());
            throw new UpstreamException("Error opening the upstream, " + e.getMessage());
        }

        return is;
    }

    void readAndSend(InputStream is, OutputStream os) throws IOException {
        byte[] b = new byte[4096];
        int length;
        while ((length = is.read(b)) != -1) {
            os.write(b, 0, length);
        }
    }

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {

        String upstream = getUpstream(httpExchange.getRequestURI().toString());
        log.info("Handling request " + httpExchange.getRequestURI().toString());
        if (upstream != null) {
            OutputStream os = httpExchange.getResponseBody();
            try {
                InputStream is = openUpstream(upstream);
                httpExchange.getResponseHeaders().add("Refresh", "0");
                httpExchange.sendResponseHeaders(200, 0);
                readAndSend(is, os);
                os.close();
            } catch (MalformedURLException e) {
                ErrorWriter.setClientError(httpExchange, "Specified upstream was malformed", log);
            } catch (IOException e) {
                ErrorWriter.setServerError(httpExchange, "I/O error", log);
            } catch (UpstreamException e) {
                ErrorWriter.setClientError(httpExchange, e.getMessage(), log);
            }
        } else {
            ErrorWriter.setClientError(httpExchange, "Specified upstream was incorrect", log);
        }
        log.info("Request finished" + httpExchange.getRequestURI());
    }
}
