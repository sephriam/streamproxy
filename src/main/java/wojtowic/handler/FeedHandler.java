package wojtowic.handler;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import wojtowic.common.KeyHandler;
import wojtowic.common.ErrorWriter;
import wojtowic.controller.FeedsController;
import wojtowic.model.ImageFeed;
import java.io.IOException;

public class FeedHandler extends KeyHandler implements HttpHandler {

    private static Logger log = LogManager.getLogger(FeedHandler.class);
    FeedsController feedsController;

    public FeedHandler(FeedsController feedsController) {
        this.feedsController = feedsController;
    }

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        log.info("Handling request " + httpExchange.getRequestURI().toString());
        String key = getKey(httpExchange.getRequestURI());
        if(key == null) {
            ErrorWriter.setClientError(httpExchange, "Key not found", log);
        } else {
            feedsController.setFeed(key, new ImageFeed(IOUtils.toByteArray(httpExchange.getRequestBody())));
            httpExchange.sendResponseHeaders(201, 0);
            httpExchange.getResponseBody().write("OK".getBytes());
            httpExchange.getResponseBody().close();
        }
        log.info("Request finished" + httpExchange.getRequestURI());
    }
}
