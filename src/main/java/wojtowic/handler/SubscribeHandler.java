package wojtowic.handler;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import wojtowic.common.KeyHandler;
import wojtowic.common.ErrorWriter;
import wojtowic.controller.FeedsController;
import wojtowic.model.ImageFeed;

import java.io.IOException;
import java.io.OutputStream;

public class SubscribeHandler extends KeyHandler implements HttpHandler  {

    private static Logger log = LogManager.getLogger(SubscribeHandler.class);
    FeedsController feedsController;

    public SubscribeHandler(FeedsController feedsController) {
        this.feedsController = feedsController;
    }

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {

        log.info("Handling request " + httpExchange.getRequestURI().toString());

        String key = getKey(httpExchange.getRequestURI());
        ImageFeed feed = feedsController.getFeed(key);

        if (feed == null) {
            ErrorWriter.setClientError(httpExchange, "Specified key does not exist", log);
        } else {
            httpExchange.getResponseHeaders().add("Refresh", "0");
            httpExchange.getResponseHeaders().add("Cache-Control", "no-store, no-cache");
            httpExchange.sendResponseHeaders(200, 0);
            OutputStream os = httpExchange.getResponseBody();
            os.write(feed.getData());
            os.close();
        }

        log.info("Request finished" + httpExchange.getRequestURI());
    }
}
