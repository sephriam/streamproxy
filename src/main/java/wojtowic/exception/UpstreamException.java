package wojtowic.exception;

public class UpstreamException extends Exception {

    public UpstreamException(String var1) {
        super(var1);
    }
}