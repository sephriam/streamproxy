package wojtowic;

import wojtowic.server.Server;

public class Application {

    public static void main(String[] args) throws Exception {

        int port = 8000;
        int threadsPoolSize = 32;

        try {
            port = Integer.valueOf(args[0]);
            threadsPoolSize = Integer.valueOf(args[1]);
        } catch (Exception ignored) {
        }

        Server server = Server.create(port, threadsPoolSize);
        server.serve();
    }
}