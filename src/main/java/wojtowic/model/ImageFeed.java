package wojtowic.model;

public class ImageFeed {
    private byte[] data;

    public ImageFeed(byte[] data) {
        this.data = data;
    }

    public byte[] getData() {
        return data;
    }
}
