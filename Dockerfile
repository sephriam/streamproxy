FROM openjdk:8

ADD target/streamproxy-1.0-SNAPSHOT.jar /app/streamproxy.jar

ENTRYPOINT [ "java" ]
CMD ["-jar", "/app/streamproxy.jar"]
